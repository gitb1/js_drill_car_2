// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.


function filterBMWAndAudi(inventory) {
    try {
        const BMWAndAudi = inventory.filter(car => car.car_make === 'BMW' || car.car_make === 'Audi');

        // Return the array of BMW and Audi cars
        return BMWAndAudi;

        // Return the array of BMW and Audi cars
        return BMWAndAudi;
    } catch (error) {
        console.error('Error in filterBMWAndAudi function:', error.message);
        return [];
    }
}





module.exports = filterBMWAndAudi;
