// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortCarModelsAlphabetically(inventory) {
 try{
    const carModels = inventory.map(car => car.car_model);

    // Sort car models alphabetically
    const sortedCarModels = carModels.sort((a, b) => a.localeCompare(b));

    return sortedCarModels;
  }catch (error) {
    console.error('Error in sortCarModelsAlphabetically function:', error.message);
    return [];
  }
}

module.exports = sortCarModelsAlphabetically;

