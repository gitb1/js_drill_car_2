 try{
    const findLastCar = require('../problem2.js');

let inventory = require('../inventory.js');
const lastCar = findLastCar(inventory);
console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);
 }catch (error){
    console.error('Error:', error.message);
 }