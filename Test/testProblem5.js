try{
const findOlderCars = require('../problem5.js');

// Assuming the inventory data is provided as a separate array
let inventory = require('../inventory.js');

// Call the findOlderCars function with the inventory and year 2000
const olderCars = findOlderCars(inventory, 2000);

// Log the number of older cars
console.log("Number of cars older than 2000:", olderCars.length);

// Log the older cars
console.log("Older cars:");
console.log(olderCars);
}catch(error){
    console.error('Error:',error.message);
}