// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
//"Last car is a *car make goes here* *car model goes here*"

function findLastCar(inventory) {
    try {
        
        // Check if inventory is empty
        if (inventory.length === 0) {
            throw new Error('Inventory is empty');
        }

        const lastCar = inventory
        .filter((car, index, arr) => index === arr.length - 1)
        .map(car => car)[0];

        return lastCar;
      } catch (error) {
         console.error('Error in findLastCar function:', error.message);
         return null;
     }
   }

module.exports = findLastCar;