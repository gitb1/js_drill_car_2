// Function to find car by id
function findCarById(inventory, id) {
    try {
        
        // Check if id is provided
        if (id === undefined || id === null) {
            throw new Error('ID is required');
        }
        const foundCar = inventory.find(car => car.id === id);
        
        if (foundCar) {
            return foundCar;
        } else {
            return null;
        }
    
    } catch (error) {
        console.error('Error in findCarById function:', error.message);
        return null;
    }
}

module.exports = findCarById;