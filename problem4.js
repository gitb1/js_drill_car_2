// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function getCarYears(inventory) {
    try {
        // Extract car years from inventory
        const years = inventory.map(car => car.car_year);
        return years;
    } catch (error) {
        console.error('Error in getCarYears function:', error.message);
        return [];
    }
}

module.exports = getCarYears;