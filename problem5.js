// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.


function findOlderCars(inventory, year) {
    try {
        // Check if year is provided and is a number
        if (typeof year !== 'number' || isNaN(year)) {
            throw new Error('Year should be a valid number');
        }

        // Iterate through each car in the inventory
        const olderCars = inventory.filter(car => car.car_year < year);

        // Return the array of older cars
        return olderCars;
        

    } catch (error) {
        console.error('Error in findOlderCars function:', error.message);
        return [];
    }
}

module.exports = findOlderCars;
